#!/bin/sh

export OPENSHIFT_CLUSTER_ID="starter-us-east-1"

cat /run/secrets/dockerloginpassword | docker login --username geircode --password-stdin
cd /app

oc login https://api.$OPENSHIFT_CLUSTER_ID.openshift.com --token="$(< /run/secrets/oc_login_token)"
# docker login registry.$OPENSHIFT_CLUSTER_ID.openshift.com -u $(oc whoami) -p $(oc whoami -t)

docker login registry.starter-us-east-1.openshift.com -u $(oc whoami) -p $(oc whoami -t)

# docker pull registry.starter-us-east-1.openshift.com/geircode-online-cli/openshift-online-cli
# docker pull registry.starter-us-east-1.openshift.com/<project>/<image>:<tag>

# wait forever
tail -f /dev/null