#!/bin/sh

cat /run/secrets/dockerloginpassword | docker login --username geircode --password-stdin
cd /app
docker build -f Dockerfile -t geircode/openshift_online_cli:latest .
docker push geircode/openshift_online_cli:latest
oc login https://api.starter-us-east-1.openshift.com --token="$(< /run/secrets/oc_login_token)"
oc import-image openshift-online-cli --from=geircode/openshift_online_cli --confirm --scheduled=true

# wait forever
tail -f /dev/null