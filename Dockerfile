FROM centos

RUN yum -y update; yum clean all

# https://fedoraproject.org/wiki/EPEL
RUN yum -y install epel-release; yum clean all

RUN yum -y install wget openssl

RUN mkdir install \
  && cd /install \
  && wget https://github.com/openshift/origin/releases/download/v3.9.0/openshift-origin-client-tools-v3.9.0-191fece-linux-64bit.tar.gz \
  && tar -xvzf openshift-origin-client-tools-v3.9.0-191fece-linux-64bit.tar.gz \
  && cd openshift-origin-client-tools-v3.9.0-191fece-linux-64bit \
  && mv oc /usr/local/bin/ \
  && rm -rf oc oc.tar.gz

# # https://docs.docker.com/engine/reference/builder/#user
USER 1001

ENTRYPOINT tail -f /dev/null