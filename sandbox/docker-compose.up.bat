cd %~dp0
docker rm -f openshift_online_cli_sandbox_1
docker-compose -f docker-compose.yml down --remove-orphans
REM docker-compose -f docker-compose.yml build --no-cache
docker-compose -f docker-compose.yml up --build --remove-orphans
pause
docker exec -it openshift_online_cli_sandbox_1 /bin/bash