cd /d %~dp0
docker rm -f openshift_online_cli_1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up -d --build
pause
docker exec -it openshift_online_cli_1 /bin/bash